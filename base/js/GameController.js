var App = App || {};
App.GameController = (function () {
    "use strict";
    /* eslint-env browser */
    var seconds = 0,
        minutes = 0,
        playing = true,
        timeView,
        board;

    function setTime() {
        if (!playing) {
            return;
        }
        seconds++;
          if(seconds > 59){
            seconds = 0;
            minutes++;
            if (minutes <=9){
                minutes ="0"+minutes;
            }
        }
            if(seconds <=9 ){
                seconds ="0"+seconds;
            }
        timeView.innerHTML = minutes+":"+seconds;
    }

    function init() {
        timeView = document.querySelector("#time");
        board = new App.BoardController();
        board.init();
        setInterval(setTime, 1000);
    }

    return {
        init: init
    };

}());
