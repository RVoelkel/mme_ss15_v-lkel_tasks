var App = App || {};
App.BoardController = function () {
    "use strict";
    /* eslint-env browser */
    var CARD_TURN_DELAY = 500,
        items = ["clock", "eye", "toolbox", "forkknife", "gear", "planet", "skull", "tshirt", "clock", "eye", "toolbox", "forkknife", "gear", "planet", "skull", "tshirt"],
        score = 0,
        scoreView = document.querySelector("#score"),
        NUMBER_CARDS = 16,
		successView = document.querySelector("#success");
    

    function turnCard(card) {
        setTimeout(function () {
            card.classList.toggle("open");
        }, CARD_TURN_DELAY);
    }

    function checkCards(openCards) {
        var firstItem, secondItem;

        firstItem = openCards[0].getAttribute("item");
        secondItem = openCards[1].getAttribute("item");

        if (firstItem === secondItem) {
            return true;
        } else {
            return false;
        }
    }

    function solveCard(card, listenerToRemove) {
        card.removeEventListener("click", listenerToRemove, false);
        card.classList.remove("open");
        card.classList.add("solved");
    }

    function onCardClicked(event) {
        var openCards;
        event.target.classList.toggle("open");
        openCards = document.querySelectorAll("#board .open");
        if (openCards.length === 2) {
            if (checkCards(openCards) === true) {
                solveCard(openCards[0], onCardClicked);
                solveCard(openCards[1], onCardClicked);
				counter();
               
            } else {
                turnCard(openCards[0]);
                turnCard(openCards[1]);
            }
        }

    }


    function counter() {
		score = score + 2;
        scoreView.innerHTML = score + "/" + NUMBER_CARDS;
		if (score === 16) {
			successView.className = "visible";
		}
			
	}
	


    function init() {
        var card;
        items.shuffle();
        items.forEach(function (item) {
            card = document.createElement("span");
            card.classList.add("card");
            card.setAttribute("item", item);
            card.style.backgroundImage = "url('img/" + item + ".png')";
            card.addEventListener("click", onCardClicked, false);
            document.querySelector("#board").appendChild(card);
            
        });
    }

    return {
        init: init
    };


};
